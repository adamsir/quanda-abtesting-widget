/* 
 * 
 * WIDGET FOR QUANDA Platform AB Testing
 * Main functions
 * 
 * 1. update - base function for redrawing the widget
 * 2. initState - base function for hooking first state of the widget (event - DOMContentLoaded)
 *
 */

$(function() {

/* Core objects
----------------------------------*/

var _ = jQuery.prototype;

var WIDGET_SELECTOR = $('#abtesting-widget'),
    WIDGET_INPUTS,
    WIDGET_WRAPPER,
    WIDGET_SETTINGS,
    RECIPIENTS,
    RECIPIENTS_WINNER,
    currentStep = 0;


/* Common Jquery functions
----------------------------------*/

_.updateContent = function(val) {
  this.each(function(e) {
    $(this).html(val);
  })
};

_.updateInput = function(val) {
  this.val(val);
}

_.getStepWidth = function() {
  if (currentStep !== 'undefined') {
    currentStep = this.width();
    return currentStep;
  }
};


/* Number functions
----------------------------------*/

round = function(val) {
  return Math.round(parseFloat(val));
};

formatNumber = function(val) {
  var result,
    thousands = val / 1000,
    milions = val / 1000000;

  if (thousands >= 1 && thousands < 1000) {
    result = parseFloat(thousands).toFixed(1) + 'k';
  } else if (milions >= 1) {
    result = parseFloat(milions).toFixed(2) + 'M';
  } else {
    result = val;
  }

  return result;
};


/* Functions for calculating interface
----------------------------------*/

checkLimits = function(width) {
  var result;

  if (width <= WIDGET_SETTINGS.minWidth) {
    result = WIDGET_SETTINGS.minWidth;
  } else if (width > WIDGET_SETTINGS.maxWidth) {
    result >= WIDGET_SETTINGS.maxWidth;
  } else {
    result = width;
  }

  return result;
};

// Take step width and return how much percentage is it from width of RECIPIENTS
getStepPercentage = function(parentWidth, width) {
  var percentage = round(width / parentWidth * 100);
  return percentage;
}

calcWidth = function(width, percentage) {
  return width / 100 * percentage;
};

calcStep = function(wrapperWidth, cellWidth) {
  return getStepPercentage(wrapperWidth, cellWidth);
};

/*calcRecipients = function() {
  var recipients = WIDGET_INPUTS.recipientsCount.val(),
      recipientSize = WIDGET_INPUTS.sampleSize.val(),
      result = {};

  result = {
    recipient: (recipientSize / 100) * recipients,
    recipientWinner: ((100 - recipientSize) / 100) * recipients
  }

  return result;
}*/

  calcRecipients = function () {
    var totalRecipients = WIDGET_INPUTS.recipientsCount.val(),
        sampleRatio = WIDGET_INPUTS.sampleSize.val(),
        result, sampleSize, winnerSize;

    sampleSize = Math.ceil(sampleRatio*totalRecipients/100/2)*2;
    //winnerSize = ((100 - sampleRatio)/100) * recipients;

    winnerSize = totalRecipients - sampleSize;

    result = {
      recipient:  sampleSize,
      recipientWinner: winnerSize
    };

    return result;
  };


/* Base function for redrawing the widget
----------------------------------*/

_.update = function() {
  var currentStepWidth = this.getStepWidth(),
      recipientsWinnerWidth,
      recipientValue,
      recipientCellValue,
      recipientWinnerValue,
      recipientWinnerCellValue,
      recipientResults;

  recipientResults = calcRecipients();

  recipientValue = recipientResults.recipient;
  recipientWinnerValue = recipientResults.recipientWinner;

  RECIPIENTS_WINNER
    .panel
    .width(WIDGET_WRAPPER.width() - RECIPIENTS.panel.width());

  recipientsWinnerWidth = RECIPIENTS_WINNER.panel.width();

  recipientCellValue = calcStep(WIDGET_WRAPPER.width(), currentStepWidth) / 2;
  recipientWinnerCellValue = calcStep(WIDGET_WRAPPER.width(), recipientsWinnerWidth);

  RECIPIENTS
    .output.recipientPercentage
    .updateContent(recipientCellValue);

  RECIPIENTS
    .output.recipientCount
    .updateContent(round(recipientValue));

  RECIPIENTS_WINNER
    .output.recipientPercentage
    .updateContent(recipientWinnerCellValue);

  RECIPIENTS_WINNER
    .output.recipientCount
    .updateContent(round(recipientWinnerValue));

  WIDGET_INPUTS
    .sampleSize
    .updateInput(recipientCellValue * 2);

};


/* Base function for hooking first state of the widget (event - DOMContentLoaded)
----------------------------------*/

_.initState = function() {
  var recipientSize = WIDGET_INPUTS.sampleSize.val(),
      recipientCellWidth = calcWidth(WIDGET_WRAPPER.width(), recipientSize);

  RECIPIENTS
    .panel
    .width(checkLimits(recipientCellWidth));

  this.update();
};


/* Data structure
----------------------------------*/

WIDGET_WRAPPER = $("#abtesting-widget");

WIDGET_SETTINGS = {
  handles: 'e, w',
  grid: calcWidth(WIDGET_WRAPPER.width(), 1),
  minWidth: calcWidth(WIDGET_WRAPPER.width(), 10), // 10% from widget width
  maxWidth: calcWidth(WIDGET_WRAPPER.width(), 90), // 90% from widget width
};

WIDGET_INPUTS = {
  recipientsCount: $('#id_recipients_count'),
  sampleSize: $('#id_sample_size')
};

RECIPIENTS = {
  panel: $('#abtesting-recipients'),
  output: {
    recipientCount: $('.js-recipient-count'),
    recipientPercentage: $('.js-recipient-percentage')
  }
};

RECIPIENTS_WINNER = {
  panel: $('#abtesting-recipients-result'),
  output: {
    recipientCount: $('.js-winning-recipient-count'),
    recipientPercentage: $('.js-winning-recipient-percentage')
  }
};


/* Init jQuery Resizable
----------------------------------*/

RECIPIENTS.panel.resizable(WIDGET_SETTINGS);
RECIPIENTS.panel.resize(function(e) {
  $(this).update();
});


/* Set initial state of the widget
----------------------------------*/

RECIPIENTS.panel.initState();


});